package main.Controls;

import main.GameField;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyControl implements KeyListener {

    //we dont add this method to the player so that we have these controls even if the player is not on the screen
    GameField field;
    public KeyControl(GameField field){
        this.field = field;
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        field.keyPressed(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        field.keyReleased(e);
    }

    }
