package main.Controls;

import Classes.EntityA;
import Classes.EntityB;
import Classes.EntityC;
import Classes.EntityD;

import java.util.LinkedList;

public class Boundaries {

    // Detects Collision between the player and every other game object
    public static boolean Collision(EntityA enta, LinkedList<EntityB> entb, LinkedList<EntityC> entc, LinkedList<EntityD> entd){
        for(int i = 0; i<entb.size(); i++){
            if(enta.getBounds().intersects(entb.get(i).getBounds()) ){
                return true;
            }
        }
        for(int i = 0; i<entc.size(); i++){
            if(enta.getBounds().intersects(entc.get(i).getBounds()) ){
                return true;
            }
        }
        for(int i = 0; i<entd.size(); i++){
            if(enta.getBounds().intersects(entd.get(i).getBounds()) ){
                return true;
            }
        }
        return false;
    }


    // We would need other types of collision in the next phases.
  /*  public static boolean Collision(EntityB entb, LinkedList<EntityA> enta){
        for(int i = 0; i<enta.size(); i++){
            if(entb.getBounds().intersects(enta.get(i).getBounds()) ){
                return true;
            }
        }
        return false;
    }*/
}
