package main.Controls;

import main.GameField;
import main.SaveLoad.FileHandler;
import main.SaveLoad.SaveData;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

public class MouseControls implements MouseListener{

    GameField field;
    public MouseControls(GameField field){
        this.field = field;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

        int mx=e.getX();
        int my=e.getY();

        if(mx>=450 && mx<=650) {
            if (my>=640 && my<=690){
                try {
                    FileHandler.Save(new SaveData(field.getPlayer(), field.getEnemy(), field.getBomb(), field.getBarrier()), "SavedGames\\sdfsdf");
                } catch(Exception exp){
                    exp.printStackTrace();
                }
            } else if (my >= 700 && my <= 750) {
                field.setState(GameField.STATE.GAME);
            } else if (my >= 760 && my <= 810) {
                try{
                    //FileHandler.Load();
                } catch (Exception exp){
                    exp.printStackTrace();
                }
            } else if (my >= 820 && my <= 870) {
                System.exit(1);
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }
}
