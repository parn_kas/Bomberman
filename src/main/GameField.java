package main;
import Classes.EntityA;
import Classes.EntityB;
import Classes.EntityC;
import Classes.EntityD;
import main.Controls.Dropper;
import main.Controls.KeyControl;
import main.Controls.MouseControls;
import main.GameObjects.Bomb;
import main.GameObjects.Player;
import main.GameObjects.Stone;
import main.GameObjects.Wall;
import main.Graphics.BackgroundCreator;
import main.Graphics.BufferedImageLoader;
import main.Graphics.Menu;
import main.Graphics.Textures;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;

public class GameField extends Canvas implements Runnable{

    private static final long serialVersionUID = 1L;
    public static final int WIDTH = 2480;
    public static final int HEIGHT = 1600;
    public static final int SCALE = 2;
    public final String TITLE = "Bomberman 1.0";

    private boolean running = false;
    private Thread thread;

    // Buffers the whole window
    private BufferedImage rickSprite = null;
    private BufferedImage mortySprite = null;
    private BufferedImage terrySprite = null;
    private BufferedImage planetSprite = null;
    private BufferedImage cromulSprite = null;

    private boolean has_dropped = false;

    private int enemy_count= 1;
    private int ememy_killed = 0;
    //private int bomb_count = 1;
    //private int bomb_exploded = 0;

    private Player rick;
    private Dropper dropper;
    private Textures tex;
    private BackgroundCreator backGround;

    private double[][] wall_map;
    private int[][] wall_type;
    private double [][] stone_map;
    private int[][] stone_type;

    public LinkedList<EntityA> ea;
    public LinkedList<EntityB> eb;
    public LinkedList<EntityC> ec;
    public LinkedList<EntityD> ed;

    public enum STATE{
        MENU,
        GAME
    };

    private STATE state = STATE.MENU;
    private main.Graphics.Menu menu;

    private int col = 20;
    private int row = 10;

    private void init(){

        requestFocus();

        BufferedImageLoader loader = new BufferedImageLoader();

        try{
            rickSprite = loader.loadImage("\\res\\RickSpriteSheet.png");
            mortySprite = loader.loadImage("\\res\\MortySpriteSheet.png");
            terrySprite = loader.loadImage("\\res\\TerrySpriteSheet.png");
            planetSprite = loader.loadImage("\\res\\PlanetSpriteSheet.png");
            cromulSprite = loader.loadImage("\\res\\CromulonSpriteSheet.png");
        } catch(IOException e){
            e.printStackTrace();
        }

        addKeyListener(new KeyControl(this));
        addMouseListener(new MouseControls(this));

        tex = new Textures(this);
        rick = new Player(200,200,this, tex);
        dropper = new Dropper(tex,this);
        backGround = new BackgroundCreator(this, tex);

        ea = dropper.getEntityA();
        eb = dropper.getEntityB();
        ec = dropper.getEntityC();
        ed = dropper.getEntityD();

        wall_map = backGround.getInitWall();
        wall_type = backGround.getWallType();
        stone_map = backGround.getStoneLoc();
        stone_type = backGround.getStoneType();

        for (int r = 1; r <= row; r++) {
            for (int c = 1; c <= col; c++) {
                if(wall_map[r-1][c-1]==1)
                    dropper.addEntity(new Wall(124*(c-1),160*(r-1),tex,this,dropper));
                if(stone_map[r-1][c-1]==1)
                    dropper.addEntity(new Stone(124*(c-1),160*(r-1),tex,this,dropper));
            }
        }

        menu = new Menu();

    }


    private synchronized void start(){
        if(running)
            return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    private synchronized void stop(){
        if(!running) {
            return;
        }
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        System.exit(1);
    }

    public void run() {
        init();
        long lastTime = System.nanoTime();
        final double amountofTicks = 60.0;
        double ns = 1000000000/amountofTicks;
        double delta = 0;
        int updates = 0;
        int frames = 0;
        long timer = System.currentTimeMillis();

        while (running) {
            // Game Loop: Updates the panel
            long now = System.nanoTime();
            delta +=(now-lastTime)/ns;
            lastTime = now;
            if(delta >=1){
                tick();
                updates++;
                delta--;
            }
            render();
            frames++;

            if(System.currentTimeMillis()-timer>1000){
                timer += 1000;
                System.out.println(updates + "Ticks, Fps" + frames);
                updates = 0;
                frames = 0;
            }
        }
        stop();
    }


    private void tick(){
        // Everything in the game that updates
        if(state==STATE.GAME){
        rick.tick();
        dropper.tick();
        }
    }

    private void render(){
        // Everything in the game that renders
        BufferStrategy bs = this.getBufferStrategy(); //access from canvas
        if (bs == null){
            //  implement tripple buffering (increases speed)
            createBufferStrategy(3);
            return;
        }
        Graphics g = bs.getDrawGraphics();
        //////////////// Drawing Stuff here
        backGround.render(g);
        if(state ==STATE.GAME) {
            rick.render(g);
            dropper.render(g);
        }else if(state == STATE.MENU){
            menu.render(g);
        }
        //////////////////////
        g.dispose();
        bs.show();

    }


    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        if(state==STATE.GAME){
        if(key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D){
            rick.setVelX(5);
        } else if(key == KeyEvent.VK_LEFT || key == KeyEvent.VK_A){
            rick.setVelX(-5);
        }else if(key == KeyEvent.VK_DOWN || key == KeyEvent.VK_S){
            rick.setVelY(5);
        }else if(key == KeyEvent.VK_UP || key == KeyEvent.VK_W) {
            rick.setVelY(-5);
        }else if (key == KeyEvent.VK_B || key == KeyEvent.VK_SPACE)
            // here I can limit how rick drops morty-bombs
            if(!has_dropped) {
                has_dropped = true;
                dropper.addEntity(new Bomb(rick.getX(), rick.getY(), tex,this));
            }
        }
    }

    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D){
            rick.setVelX(0);
        } else if(key == KeyEvent.VK_LEFT || key == KeyEvent.VK_A){
            rick.setVelX(0);
        }else if(key == KeyEvent.VK_DOWN || key == KeyEvent.VK_S){
            rick.setVelY(0);
        }else if(key == KeyEvent.VK_UP || key == KeyEvent.VK_W) {
            rick.setVelY(0);
        } else if(key == KeyEvent.VK_B || key == KeyEvent.VK_SPACE){
            if(has_dropped)
                has_dropped = false;
        }
    }

    public static void main(String args[]){

        GameField field = new GameField();

        field.setPreferredSize(new Dimension(WIDTH,HEIGHT));
        field.setMaximumSize(new Dimension(WIDTH*SCALE, HEIGHT*SCALE));
        field.setMinimumSize(new Dimension(WIDTH/SCALE, HEIGHT/SCALE));

        JFrame frame = new JFrame(field.TITLE);
        frame.add(field);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        field.start();

    }

    public BufferedImage getRickSpriteSheet(){
        return rickSprite;
    }
    public BufferedImage getMortySpriteSheet() {
        return mortySprite;
    }
    public BufferedImage getTerrySpriteSheet(){
        return terrySprite;
    }
    public BufferedImage getCromulSprite() {
        return cromulSprite;
    }
    public BufferedImage getPlanetSprite() {
        return planetSprite;
    }

    public STATE getState(){
        return state;
    }
    public void setState(STATE state){
        this.state=state;
    }

    public int[][] getWallType(){
        return wall_type;
    }
    public int[][] getStoneType(){
        return stone_type;
    }

    public int getCol(){
        return col;
    }
    public int getRow(){
        return row;
    }



    public EntityA getPlayer(){
        return rick;
    }

    public LinkedList<EntityB> getEnemy() {
        return eb;
    }

    public LinkedList<EntityC> getBomb() {
        return ec;
    }

    public LinkedList<EntityD> getBarrier() {
        return ed;
    }
}
