package main.GameObjects;

import Classes.EntityB;
import libs.Animation;
import main.*;
import main.Controls.Boundaries;
import main.Controls.Dropper;
import main.Graphics.Textures;

import java.awt.*;

public class Enemy extends GameObject implements EntityB{

    private Textures tex;
    Animation anim;
    private GameField field;
    private Dropper d;

    public Enemy(double x,double y, Textures tex, GameField field, Dropper d){
        super(x,y);
        this.tex = tex;
        anim = new Animation(5, tex.enemy[0], tex.enemy[1], tex.enemy[2], tex.enemy[3]);
        this.field = field;
        this.d = d;
    }

    public void tick(){
      /*  if(Boundaries.Collision(this,field.ea)){
            d.removeEntity(this);
            //you should remove the bomb as well
        }
        */
        anim.runAnimation();
    }

    public void render(Graphics g) {
        anim.drawAnimation(g, x, y, 0);
    }

    public Rectangle getBounds(){
        return new Rectangle((int)x,(int)y,124,160);
    }

    public Rectangle getBoundsRight() {
        return new Rectangle((int)x+124-5,(int)y+5,5,160-10);
    }

    public Rectangle getBoundsLeft() {
        return new Rectangle((int)x,(int)y+5,5,160-10);
    }

    public Rectangle getBoundsTop() {
        return new Rectangle((int)x+31,(int)y,124,80); //124/4 = 31
    }

    public Rectangle getBoundsBottom() {
        return new Rectangle((int)x+31,(int)y+80,124,80);
    }

    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }

}
