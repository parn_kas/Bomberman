package main.GameObjects;


import Classes.EntityD;
import libs.Animation;
import main.Controls.Boundaries;
import main.Controls.Dropper;
import main.GameField;
import main.GameObject;
import main.Graphics.Textures;

import java.awt.*;
import java.util.Random;

public class Wall extends GameObject implements EntityD {

    private Textures tex;
    private GameField field;
    private Dropper d;
    private Random r;

    private int type;

    public Wall(double x,double y, Textures tex, GameField field, Dropper d){
        super(x,y);
        this.tex = tex;
        this.field = field;
        this.d = d;
        type = field.getWallType()[(int)y/160][(int)x/124];

    }

    public void tick(){
    }

    public void render(Graphics g) {
        g.drawImage(tex.wall[type],(int)x,(int)y,null);
    }

    public Rectangle getBounds(){
        return new Rectangle((int)x,(int)y,124,160);
    }

    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }

}
