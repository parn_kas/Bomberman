package main.GameObjects;

import Classes.EntityC;
import libs.Animation;
import main.GameField;
import main.GameObject;
import main.Graphics.Textures;

import java.awt.*;

public class Bomb extends GameObject implements EntityC {
    private Textures tex;
    private GameField field;
    //you can define a random explosion time for each bomb here, or in tick method

    Animation anim;
    public Bomb(double x, double y, Textures tex, GameField field) {
        super(x,y);
        this.tex = tex;
        this.field = field;
        anim = new Animation(5,tex.bomb[0],tex.bomb[1],tex.bomb[2],tex.bomb[3]);
    }

    public void tick() {

        anim.runAnimation();
    }

    public void render(Graphics g) {
        anim.drawAnimation(g,x,y,0);
    }

    public Rectangle getBounds(){
        return new Rectangle((int)x,(int)y,124,160);
    }

    public Rectangle getBoundsRight() {
        return new Rectangle((int)x+124-5,(int)y+5,5,160-10);
    }

    public Rectangle getBoundsLeft() {
        return new Rectangle((int)x,(int)y+5,5,160-10);
    }

    public Rectangle getBoundsTop() {
        return new Rectangle((int)x+31,(int)y,124,80); //124/4 = 31
    }

    public Rectangle getBoundsBottom() {
        return new Rectangle((int)x+31,(int)y+80,124,80);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

}