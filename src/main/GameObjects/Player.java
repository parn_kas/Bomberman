package main.GameObjects;

import Classes.EntityA;
import Classes.EntityB;
import Classes.EntityC;
import Classes.EntityD;
import libs.Animation;
import main.Controls.Boundaries;
import main.GameField;
import main.GameObject;
import main.Graphics.Textures;

import java.awt.*;
import java.util.LinkedList;

public class Player extends GameObject implements EntityA {


    private GameField field;
    private double x;
    private double y;

    private double velX = 0;
    private double velY= 0;

    private Textures tex;

    Animation anim;

    public Player(double x, double y, GameField field, Textures tex){
        super(x,y);
        this.field = field;
        this.tex = tex;

        anim = new Animation(5,tex.player[0],tex.player[1],tex.player[2],tex.player[3]);
    }


    private void Obstacle(){

        LinkedList<EntityB> entb = field.eb;
        LinkedList<EntityC> entc = field.ec;
        LinkedList<EntityD> entd = field.ed;

        Rectangle tempRect;
        double tempX,tempY;

        for(int i = 0; i<entb.size(); i++){

            tempRect = entb.get(i).getBounds();
            tempX = entb.get(i).getX();
            tempY = entb.get(i).getY();

            if(this.getBoundsLeft().intersects(tempRect)){
                velX = 0;
                x = tempX+124;
            }
            if(this.getBoundsRight().intersects(tempRect) ){
                velX = 0;
                x = tempX-124;
            }
            if(this.getBoundsTop().intersects(tempRect) ){
                velY = 0;
                y = tempY+160;
            }
            if(this.getBoundsBottom().intersects(tempRect) ){
                velY = 0;
                y = tempY-160;
            }
        }

        for(int i = 0; i<entc.size(); i++){

            tempRect = entc.get(i).getBounds();
            tempX = entc.get(i).getX();
            tempY = entc.get(i).getY();

            if(this.getBoundsLeft().intersects(tempRect)){
                velX = 0;
                x = tempX+124;
            }
            if(this.getBoundsRight().intersects(tempRect) ){
                velX = 0;
                x = tempX-124;
            }
            if(this.getBoundsTop().intersects(tempRect) ){
                velY = 0;
                y = tempY+160;
            }
            if(this.getBoundsBottom().intersects(tempRect) ){
                velY = 0;
                y = tempY-160;
            }
        }
        for(int i = 0; i<entd.size(); i++){

            tempRect = entd.get(i).getBounds();
            tempX = entd.get(i).getX();
            tempY = entd.get(i).getY();

            if(this.getBoundsLeft().intersects(tempRect)){
                velX = 0;
                x = tempX+124;
            }
            if(this.getBoundsRight().intersects(tempRect) ){
                velX = 0;
                x = tempX-124;
            }
            if(this.getBoundsTop().intersects(tempRect) ){
                velY = 0;
                y = tempY+160;
            }
            if(this.getBoundsBottom().intersects(tempRect) ){
                velY = 0;
                y = tempY-160;
            }
        }



    }

    public void tick(){

            x += velX;
            y += velY;

        if(x<=0)
            x = 0;
        if(x>field.WIDTH - 124)
            x = field.WIDTH - 124; //Canvas size - player size
        if(y<0)
            y = 0;
        if(y>field.HEIGHT - 160)
            y = field.HEIGHT - 160;

        Obstacle();

        anim.runAnimation();
    }
    public void render(Graphics g){
        anim.drawAnimation(g,x,y,0);
    }

    public Rectangle getBounds(){
        return new Rectangle((int)x,(int)y,124,160);
    }

    public Rectangle getBoundsRight() {
        return new Rectangle((int)x+124-5,(int)y+5,5,160-10);
    }

    public Rectangle getBoundsLeft() {
        return new Rectangle((int)x,(int)y+5,5,160-10);
    }

    public Rectangle getBoundsTop() {
        return new Rectangle((int)x+31,(int)y,62,80); //124/4 = 31
    }

    public Rectangle getBoundsBottom() {
        return new Rectangle((int)x+31,(int)y+80,62,80);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getVelX() {
        return velX;
    }

    public void setVelX(double velX) {
        this.velX = velX;
    }

    public double getVelY() {
        return velY;
    }

    public void setVelY(double velY) {
        this.velY = velY;
    }

}
