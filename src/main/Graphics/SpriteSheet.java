package main.Graphics;

import java.awt.image.BufferedImage;

public class SpriteSheet{

// The Sprite sheet contains different elements of the games graphics.
// In this class, we load the sprite sheet and grab the bomberman, bombs, etc from the sheet

    private BufferedImage image;
    public SpriteSheet(BufferedImage image){
        this.image = image;
    }
    public BufferedImage grabImage(int biasX, int biasY, int col, int row, int width, int height){
        // x: 5 , y: 735 for rick
        // x: 4, y: 656 for morty
        BufferedImage img = image.getSubimage(biasX+(col*width)-width,biasY+(row*height)-height,width,height);
        return img;
    }
}
