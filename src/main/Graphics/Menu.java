package main.Graphics;

import java.awt.*;

public class Menu {

    private Rectangle saveButton = new Rectangle(450,640,200,50);
    private Rectangle playButton = new Rectangle(450,700, 200,50);
    private Rectangle loadButton = new Rectangle(450,760, 200,50);
    private Rectangle quitButton = new Rectangle(450,820, 200,50);


    public void render(Graphics g){
        Graphics2D g2d = (Graphics2D) g;

        Font fnt0 = new Font("halvatica", Font.BOLD, 60);
        Font fnt1 = new Font("halvatica", Font.BOLD, 40);
        g.setFont(fnt0);
        g.setColor(Color.YELLOW);
        g.drawString("SHNOOPS DOOPS", 300,400);
        g.setFont(fnt1);
        g.setColor((Color.orange));
        g.drawString("A RICK & MORTY THEMED BOMBERMAN",200,450);

        g.drawString("Save",saveButton.x+50,saveButton.y+35);
        g.drawString("Play",playButton.x+50,playButton.y+35);
        g.drawString("Load",loadButton.x+50,loadButton.y+35);
        g.drawString("Quit",quitButton.x+50,quitButton.y+35);

        g2d.draw(saveButton);
        g2d.draw(playButton);
        g2d.draw(loadButton);
        g2d.draw(quitButton);
    }
}
