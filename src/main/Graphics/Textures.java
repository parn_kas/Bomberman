package main.Graphics;

import main.GameField;
import java.awt.image.BufferedImage;

public class Textures {

    public BufferedImage[] player = new BufferedImage[4];
    public BufferedImage[] bomb = new BufferedImage[4];
    public BufferedImage[] enemy = new BufferedImage[4];
    public BufferedImage[] wall = new BufferedImage[4];
    public BufferedImage[] stone = new BufferedImage[5];

    private SpriteSheet rickSS;
    private SpriteSheet mortySS;
    private SpriteSheet terrySS;
    private SpriteSheet planetSS;
    private SpriteSheet cromulSS;

    public Textures(GameField field){

        rickSS = new SpriteSheet(field.getRickSpriteSheet());
        mortySS = new SpriteSheet(field.getMortySpriteSheet());
        terrySS = new SpriteSheet(field.getTerrySpriteSheet());
        planetSS = new SpriteSheet(field.getPlanetSprite());
        cromulSS = new SpriteSheet(field.getCromulSprite());
        getTextures();
    }

    private void getTextures(){
        player[0] = rickSS.grabImage(5,736,1,1,124,160);
        player[1] = rickSS.grabImage(10,736,2,1,124,160);
        player[2] = rickSS.grabImage(15,736,3,1,124,160);
        player[3] = rickSS.grabImage(20,736,4,1,124,160);

        bomb[0] = mortySS.grabImage(4, 656, 1, 1, 124, 160);
        bomb[1] = mortySS.grabImage(10, 656, 2, 1, 124, 160);
        bomb[2] = mortySS.grabImage(14, 656, 3, 1, 124, 160);
        bomb[3] = mortySS.grabImage(19, 656, 4, 1, 124, 160);

        enemy[0] = terrySS.grabImage(5,695, 1, 1, 124, 160);
        enemy[1] = terrySS.grabImage(11,695, 2, 1, 124, 160);
        enemy[2] = terrySS.grabImage(17,695, 3, 1, 124, 160);
        enemy[3] = terrySS.grabImage(23,695, 4, 1, 124, 160);

        wall[0] = planetSS.grabImage(0,0,1,1,124,160);
        wall[1] = planetSS.grabImage(0,0,1,2,124,160);
        wall[2] = planetSS.grabImage(0,0,2,1,124,160);
        wall[3] = planetSS.grabImage(0,0,2,2,124,160);

        stone[0] = cromulSS.grabImage(0,0,1,1,124,160);
        stone[1] = cromulSS.grabImage(0,0,1,2,124,160);
        stone[2] = cromulSS.grabImage(0,0,2,1,124,160);
        stone[3] = cromulSS.grabImage(0,0,2,2,124,160);
        stone[4] = cromulSS.grabImage(0,0,3,1,124,160);

    }
}
