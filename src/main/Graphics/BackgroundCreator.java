package main.Graphics;

import main.Controls.Dropper;
import main.GameField;
import main.GameObjects.Wall;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

public class BackgroundCreator{

    private BufferedImage tile1;
    private BufferedImage tile2;
    private BufferedImage menu;

    private int col;
    private int row;

    private GameField field;


    //private LinkedList<Integer> stoneCoordinates;
    private double stone_prob = 0.85;
    private double wall_prob = 0.85;


    private Random rand;
    private int[][] choose;
    private double[][] init_wall;
    private int[][] init_wall_type;
    private double[][] put;

    public BackgroundCreator(GameField field, Textures tex) {  ///later add stone_prob : sets the number of solid stones ^_^
        this.field = field;
        //this.stone_prob = stone_prob;
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        try {
            tile1 = ImageIO.read(new File(s + "\\res\\Space1.png"));
            tile2 = ImageIO.read(new File(s + "\\res\\Space2.jpg"));
            menu = ImageIO.read(new File(s+"\\res\\MenuBackground.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        rand = new Random();

        row = field.getRow();
        col = field.getCol();
        choose = new int[row][col];
        init_wall = new double[row][col];
        init_wall_type = new int[row][col];
        put = new double[row][col];

        for (int r = 1; r <= row; r++) {
            for (int c = 1; c <= col; c++) {
                choose[r-1][c-1] = rand.nextInt(5);
                init_wall_type[r-1][c-1] = rand.nextInt(4);
                put[r-1][c-1]=((rand.nextDouble()>=stone_prob)?1:0);
                init_wall[r-1][c-1]=((rand.nextDouble()>=wall_prob)?1:0);
                if(init_wall[r-1][c-1]==1 && put[r-1][c-1]==1)
                    put[r-1][c-1]=0;
            }
        }
    }


    public void render(Graphics g){

        int width= 124;
        int height = 160;

        if(field.getState()== GameField.STATE.MENU) {
            g.drawImage(menu, 0, 0, null);
        } else {
        for(int r=1; r<=row; r++){
            for(int c=1; c<=col; c++){
                if((r+c)%2==0) {
                    g.drawImage(tile1,(c-1)*width,(r-1)*height, null);
                } else{
                    g.drawImage(tile2,(c-1)*width,(r-1)*height, null);
                }
            }
            }

        }
    }

    public double[][] getInitWall(){
        return init_wall;
    }
    public int[][] getWallType(){
        return init_wall_type;
    }

    public double[][] getStoneLoc(){
        return put;
    }
    public int[][] getStoneType(){
        return choose;
    }
}
