package main.SaveLoad;

import Classes.EntityA;
import Classes.EntityB;
import Classes.EntityC;
import Classes.EntityD;

import java.io.Serializable;
import java.io.StringReader;
import java.util.LinkedList;

public class SaveData implements Serializable {

    private static final long serialVersionUID = 1L;
    public String name;
    public int hp;

    private EntityA enta;
    private LinkedList<EntityB> entb;
    private LinkedList<EntityC> entc;
    private LinkedList<EntityD> entd;

    public SaveData(EntityA enta, LinkedList<EntityB> entb, LinkedList<EntityC> entc, LinkedList<EntityD> entd){
        this.enta = enta;
        this.entb = entb;
        this.entc = entc;
        this.entd = entd;
    }


}
