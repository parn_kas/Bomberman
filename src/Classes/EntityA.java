package Classes;

import java.awt.*;

public interface EntityA {

    public void tick();
    public void render(Graphics g);
    public Rectangle getBounds();
    public Rectangle getBoundsLeft();
    public Rectangle getBoundsRight();
    public Rectangle getBoundsTop();
    public Rectangle getBoundsBottom();

    public double getX();
    public double getY();
}
